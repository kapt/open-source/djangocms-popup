��          �      �       0  	   1  .   ;     j     }     �     �  
   �  
   �     �     �     �  	   �  A  �  	   4  <   >     {     �     �     �  
   �     �     �     �  	                 
         	                                        (unknown) Allow the popup to be reopened if it is closed Bottom right popup Display after X seconds Page or static placeholder Popup Popup ({}) Popup list Popup template Popups draft published Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 (inconnu) Permettre à la popup d'être réouverte si elle est fermée Popup en bas à droite Afficher après X secondes Page ou placeholder statique Popup Popup ({}) Liste des popups Modèle de popup Popups brouillon publié 