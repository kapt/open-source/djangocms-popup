from django.apps import AppConfig


class DjangocmsPopupConfig(AppConfig):
    name = "djangocms_popup"
